## Installation & Documentation
Contact me through email jacktreea@gmail.com
Also, For complete updated documentation of the aggy pos please email me jacktreea@gmail.com
## Security Vulnerabilities

If you discover a security vulnerability within aggy POS, please send an e-mail to support at jacktreea@gmail.com. All security vulnerabilities will be promptly addressed.